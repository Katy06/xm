$(function () {
	var $body = $("body");
	var $lma = $("<div/>");
	$lma.addClass("lma");
	$lma.html("<div class=\"tree\"></div><div class=\"santa\"></div><h1>Merry Christmas</h1><h1>Tmbl!</h1><h2>&mdash; Tmbla</h2>");

	setTimeout(function () {
		$body.toggleClass("xm");
		$body.append($lma);
		$lma.animate({ opacity: 1 }, 3000, function () {
			setTimeout(function () {
				$(document).snowfall({
					flakeCount: 250, maxSpeed: 10, image: function () {
						var rnd = parseInt(Math.random() * 1000 % 19) + 1;
						return "images/flake-" + rnd + ".png";
					}, minSize: 10, maxSize: 32
				});
				var elements = document.querySelectorAll(".snowfall-flakes");
				var animationDuration = 50000;
				document.querySelectorAll(".snowfall-flakes").forEach(function(element) {
					var randomDuration = Math.floor(Math.random() * animationDuration);
					element.style.animationDelay = randomDuration + "ms";
					element.style.animationDuration = (randomDuration % 100 + 4) + "s";
					element.style.animationName = "spin-" + (randomDuration % 7 + 1);
				});
			}, 1000);
		});
	});
});